package ru.ulstu.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.ulstu.model.EduGroup;
import ru.ulstu.model.EduGroupStatistic;
import ru.ulstu.model.OffsetablePageRequest;
import ru.ulstu.model.PageableItems;
import ru.ulstu.repository.EduGroupRepository;

import java.util.List;
import java.util.Optional;

@Service
public class EduGroupService implements Crud<EduGroup> {
    private final EduGroupRepository eduGroupRepository;

    public EduGroupService(EduGroupRepository eduGroupRepository) {
        this.eduGroupRepository = eduGroupRepository;
    }

    public EduGroup create(EduGroup eduGroup) {
        return eduGroupRepository.save(eduGroup);
    }

    @Override
    public List<EduGroup> findAll() {
        return eduGroupRepository.findAll();
    }

    @Override
    public EduGroup get(Integer id) {
        return eduGroupRepository.getOne(id);
    }

    @Override
    public Optional<EduGroup> find(Integer id) {
        return eduGroupRepository.findById(id);
    }

    @Override
    public PageableItems<EduGroup> findAll(int offset, int count) {
        Page<EduGroup> page = eduGroupRepository.findAll(new OffsetablePageRequest(offset, count));
        return new PageableItems<>(page.getTotalElements(), page.getContent());
    }

    @Override
    public EduGroup update(EduGroup eduGroup) {
        return eduGroupRepository.save(eduGroup);
    }

    @Override
    public void delete(EduGroup eduGroup) {
        eduGroupRepository.delete(eduGroup);
    }

    public List<EduGroup> findByName(String name) {
        return eduGroupRepository.findByName(name);
    }

    public List<EduGroup> findByPartOfName(String partOfName) {
        return eduGroupRepository.findByPartOfName(partOfName);
    }

    public List<EduGroupStatistic> getGroupsCountByCourse(int course) {
        return eduGroupRepository.getGroupsCountByCourse(course);
    }
}
