package ru.ulstu.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.ulstu.model.EduGroup;
import ru.ulstu.model.OffsetablePageRequest;
import ru.ulstu.model.PageableItems;
import ru.ulstu.model.Student;
import ru.ulstu.repository.StudentRepository;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService implements Crud<Student> {
    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Student create(Student student) {
        return studentRepository.save(student);
    }

    public Optional<Student> find(Integer id) {
        return studentRepository.findById(id);
    }

    public Student get(Integer id) {
        return studentRepository.getOne(id);
    }

    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    public PageableItems<Student> findAll(int offset, int count) {
        final Page<Student> page = studentRepository.findAll(new OffsetablePageRequest(offset, count));
        return new PageableItems<>(page.getTotalElements(), page.getContent());
    }

    public Student update(Student student) {
        if (student.getId() == null) {
            throw new RuntimeException("Student id is null");
        }
        return studentRepository.save(student);
    }

    public void delete(Student student) {
        studentRepository.delete(student);
    }

    public List<Student> findByEduGroup(EduGroup eduGroup) {
        return studentRepository.findByEduGroup(eduGroup);
    }
}
