package ru.ulstu.model;

import javax.persistence.Entity;

@Entity
public class EduGroup extends BaseEntity {
    private String name;
    private Integer course;
    private Integer groupNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
    }
}
