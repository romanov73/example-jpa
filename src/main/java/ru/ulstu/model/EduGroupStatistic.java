package ru.ulstu.model;

public class EduGroupStatistic extends StringRepresentation {
    private EduGroup eduGroup;
    private Long count;

    public EduGroupStatistic(EduGroup eduGroup, Long count) {
        this.eduGroup = eduGroup;
        this.count = count;
    }

    public EduGroup getEduGroup() {
        return eduGroup;
    }

    public void setEduGroup(EduGroup eduGroup) {
        this.eduGroup = eduGroup;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
