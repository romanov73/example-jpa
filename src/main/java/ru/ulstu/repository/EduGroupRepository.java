package ru.ulstu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.ulstu.model.EduGroup;
import ru.ulstu.model.EduGroupStatistic;

import java.util.List;

public interface EduGroupRepository extends JpaRepository<EduGroup, Integer> {
    List<EduGroup> findByName(String name);

    @Query("SELECT g FROM EduGroup g WHERE LOWER(g.name) LIKE concat('%', :partOfName,'%')")
    List<EduGroup> findByPartOfName(@Param("partOfName") String partOfName);

    @Query("SELECT new ru.ulstu.model.EduGroupStatistic(g, COUNT(s)) FROM Student s JOIN s.eduGroup g WHERE g.course = :course GROUP BY g")
    List<EduGroupStatistic> getGroupsCountByCourse(@Param("course") int course);
}
