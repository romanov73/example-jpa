package ru.ulstu.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.model.EduGroup;
import ru.ulstu.model.Student;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Integer> {

    Page<Student> findAll(Pageable pageable);

    List<Student> findByEduGroup(EduGroup eduGroup);
}
